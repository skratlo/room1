#ifndef version_h
#define version_h
#include <string>

using namespace std;

extern const string GIT_VERSION;
extern const string GIT_VERSION_SHORT;
extern const uint8_t GIT_VERSION_MAJOR;
extern const uint8_t GIT_VERSION_MINOR;
extern const uint8_t GIT_VERSION_PATCH;

#endif
