#include <fstream>
#include <iostream>
#include <vector>
#include <cassert>

#include <filament/Engine.h>
#include <filament/IndexBuffer.h>
#include <filament/RenderableManager.h>
#include <filament/Scene.h>
#include <filament/VertexBuffer.h>
#include <filament/View.h>
#include <filament/Viewport.h>
#include <math/vec3.h>
#include <utils/EntityManager.h>

#include <SDL2/SDL.h>

using namespace std;
using namespace filament;
using namespace filament::math;

const uint32_t cube_indices[] = {
        // solid
        2,0,1, 2,1,3,  // far
        6,4,5, 6,5,7,  // near
        2,0,4, 2,4,6,  // left
        3,1,5, 3,5,7,  // right
        0,4,5, 0,5,1,  // bottom
        2,6,7, 2,7,3,  // top
};

const float3 cube_vertices[] = {
        {-1, -1,  1},  // 0. left bottom far
        { 1, -1,  1},  // 1. right bottom far
        {-1,  1,  1},  // 2. left top far
        { 1,  1,  1},  // 3. right top far
        {-1, -1, -1},  // 4. left bottom near
        { 1, -1, -1},  // 5. right bottom near
        {-1,  1, -1},  // 6. left top near
        { 1,  1, -1}}; // 7. right top near

static void resize(SDL_Window* window,
                   Engine* engine,
                   SwapChain*& swap_chain,
                   View* view,
                   Camera* camera) {
  double near = 0.1;
  double far = 50;
  uint32_t w, h;
  SDL_GL_GetDrawableSize(window, (int*) &w, (int*) &h);
  engine->destroy(swap_chain);
  swap_chain = engine->createSwapChain(window);
  camera->setProjection(45.0, double(w) / h, near, far, Camera::Fov::VERTICAL);
  view->setViewport(Viewport(0, 0, w, h));
}

static shared_ptr<vector<char>> read_file(const string& path) {
  ifstream file(path, ios::binary | ios::ate);
  streamsize size = file.tellg();
  file.seekg(0, ios::beg);

  auto buffer = make_shared<vector<char>>(size);
  if (file.read(buffer->data(), size)) {
    return buffer;
  } else {
    return nullptr;
  }
}

int main(int argc, char** argv) {
  assert(SDL_Init(SDL_INIT_EVENTS) == 0 && "SDL_Init Failure");

  static SDL_Window* window = SDL_CreateWindow
    ("SDL2Test",
     SDL_WINDOWPOS_UNDEFINED,
     SDL_WINDOWPOS_UNDEFINED,
     640, 480, 0);

  SDL_Renderer *srenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
  SDL_SetRenderDrawColor(srenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(srenderer);
  SDL_RenderPresent(srenderer);

  auto engine = Engine::create(Engine::Backend::OPENGL);
  auto swap_chain = engine->createSwapChain(window);
  auto renderer = engine->createRenderer();
  auto scene = engine->createScene();
  auto view = engine->createView();
  view->setVisibleLayers(0x4, 0x4);
  view->setClearColor({ 0 });
  view->setClearTargets(false, false, false);
  view->setRenderTarget(View::TargetBufferFlags::DEPTH_AND_STENCIL);
  view->setPostProcessingEnabled(false);
  view->setShadowsEnabled(false);
  view->setScene(scene);
  view->setName("main");

  auto camera = engine->createCamera();
  camera->setExposure(16.0f, 1 / 125.0f, 100.0f);
  camera->lookAt({4, 0, -4}, {0, 0, -4}, {0, 1, 0});
  view->setCamera(camera);
  resize(window, engine, swap_chain, view, camera);

  auto default_material_data = read_file
    ("/home/skrat/Workspace/room1/build/extern/filament/samples/generated/material/aiDefaultMat.filamat");
  if (default_material_data == nullptr) {
    cerr << "can't read material" << endl;
    exit(EXIT_FAILURE);
  }
  auto default_material = Material::Builder()
    .package(default_material_data->data(), default_material_data->size())
    .build(*engine);
  auto default_material_inst = default_material->createInstance();
  default_material_inst->setParameter
    ("baseColor", RgbaType::LINEAR, LinearColorA{1.f, 0.f, 0.f, 0.05f});
  default_material_inst->setParameter("metallic", 0.f);
  default_material_inst->setParameter("roughness", 1.f);
  default_material_inst->setParameter("reflectance", 0.f);

  auto vertex_buffer = VertexBuffer::Builder()
    .vertexCount(8)
    .bufferCount(1)
    .attribute(VertexAttribute::POSITION, 0, VertexBuffer::AttributeType::FLOAT3)
    .build(*engine);
  auto index_buffer = IndexBuffer::Builder()
    .indexCount(3*2*6)
    .build(*engine);

  vertex_buffer->setBufferAt
    (*engine, 0, VertexBuffer::BufferDescriptor
     (&cube_vertices, vertex_buffer->getVertexCount() * sizeof(float3)));

  index_buffer->setBuffer
    (*engine, IndexBuffer::BufferDescriptor
     (&cube_indices, index_buffer->getIndexCount() * sizeof(uint32_t)));

  utils::EntityManager& em = utils::EntityManager::get();
  auto solid_r = em.create();
  RenderableManager::Builder(1)
    .boundingBox({{0, 0, 0}, {1, 1, 1}})
    .material(0, default_material_inst)
    .geometry(0, RenderableManager::PrimitiveType::TRIANGLES, vertex_buffer, index_buffer, 0, 3*2*6)
    .priority(7)
    .culling(true)
    .build(*engine, solid_r);

  auto& rcm = engine->getRenderableManager();
  rcm.setLayerMask(rcm.getInstance(solid_r), 0x3, 0x2);
  scene->addEntity(solid_r);

  cout << "PRE RENDER" << endl;

  SDL_Delay(500);

  if (renderer->beginFrame(swap_chain)) {
    renderer->render(view);
    renderer->endFrame();
  }

  SDL_Delay(500);

  cout << "POST RENDER" << endl;

  rcm.destroy(solid_r);
  em.destroy(solid_r);
  engine->destroy(default_material_inst);
  engine->destroy(default_material);
  engine->destroy(index_buffer);
  engine->destroy(vertex_buffer);
  engine->destroy(camera);
  engine->destroy(view);
  engine->destroy(scene);
  engine->destroy(renderer);
  engine->destroy(swap_chain);
  engine->destroy(&engine);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
